<?php

use App\Core\Route;

function post($name)
{
    if( isset($_POST[$name]) )
    {        
        if( is_string($_POST[$name]) )
        {
            $post = htmlspecialchars( trim( $_POST[$name] ) );
        }
        elseif( is_array($_POST[$name]) || is_numeric($_POST[$name]) )
        {
            $post = $_POST[$name];
        }        
                
        if( !empty($post) )
        {
            return $post;
        }
    }
}

function redirect(string $toUrl, int $status = 301)
{
    $baseUrl = rtrim(BASE_URI, "/");
    header('Location:' .  $baseUrl . $toUrl, true, $status);
    exit();
}

function back()
{
    $redirectUrl = Route::getRedirectUrl();    
    header('Location: ' . BASE_URI . $redirectUrl);
    exit();
}


function checkAktiveUrl(string $url): bool
{
    $activeUrl = Route::getUrl();
    $activeUrl = ltrim($activeUrl, '/');  
    $array = explode('/', $activeUrl);

    if( $array[0] == $url )    
        return true;    
    else    
        return false;    
}

function checkAktiveKategorie(int $kategorie_id): bool
{
    $activeUrl = Route::getUrl();
    $array = explode('/', $activeUrl);

    if( isset($array[2]) && $kategorie_id == $array[2] && 'kategorie' == $array[1] )    
        return true;    
    else    
        return false;         
}

function checkAktiveNavMenu(string $url): bool
{
    $activeUrl = Route::getUrl();
    $activeUrl = ltrim($activeUrl, '/');  
    $array = explode('/', $activeUrl); 
    if( isset($array[1]) )
    {
        if( $array[1] == $url )        
            return true;        
        else        
            return false;        
    }
    else
    {
        if( $array[0] == $url )        
            return true;        
        else        
            return false;       

    }   
}

function isLogged(): bool
{
    if( isset($_SESSION['eingeloggt']) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isAdmin(): bool
{
    if( isset($_SESSION['eingeloggt']) && $_SESSION['user']['admin'] == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function getUserId()
{
    if( isset($_SESSION['eingeloggt']) )
    {
        return $_SESSION['user']['id'];
    }
    else
    {
        return NULL;
    }
}

function createLinks(int $countPage, string $link, int $currentPage)
{
    if( $countPage > 1 )
    {
        $lis = '';
        for($i = 1; $i <= $countPage; $i++)
        {
            $aktive = ($i == $currentPage) ? 'active' : '';
            $lis .= '<li class="page-item '. $aktive .'"><a class="page-link" href="'. $link .'/'. $i .'">'. $i .'</a></li>';
        }
        if( $currentPage != 1 )
        {
            $letzte = '<li class="page-item"><a class="page-link" href="'. $link .'/'. ($currentPage-1) .'">Letzte</a></li>';
        }
        else
        {
            $letzte = '<li class="page-item disabled"> <a class="page-link" href="" tabindex="-1">Letzte</a> </li>';
        }
        if( $currentPage != $countPage )
        {
            $next = '<li class="page-item"><a class="page-link" href="'. $link .'/'. ($currentPage+1) .'">Nächste</a></li>';
        }
        else
        {
            $next = '<li class="page-item disabled"> <a class="page-link" href="" tabindex="-1">Nächste</a> </li>';
        }    
        $html = '
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                {{ Letzte }}
                {{ Li }}
                {{ Next }}
            </ul>
        </nav>    
        ';
        $html = str_replace('{{ Li }}', $lis, $html);
        $html = str_replace('{{ Letzte }}', $letzte, $html);
        $html = str_replace('{{ Next }}', $next, $html);
        return $html;
    }
    else
    {
        return false;
    }

}

function printMessage()
{
    if( isset($_SESSION['flash']['success']) )
    {
        $html = '
        <div class="alert alert-success" role="alert">
            '. $_SESSION['flash']['success'] .'
        </div>        
        ';
    }
    if( isset($_SESSION['flash']['error']) )
    {
        $html = '
        <div class="alert alert-danger" role="alert">
            '. $_SESSION['flash']['error'] .'
        </div>        
        ';        
    }
    unset( $_SESSION['flash'] );
    return isset($html) ? $html : false;
}

function csrf_token()
{
    return "<input type='hidden' value=" .$_SESSION['_token']. " name='_token'>";
}