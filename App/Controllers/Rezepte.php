<?php

namespace App\Controllers;

use App\Core\Controller as BaseController;
use \stdClass;

class Rezepte extends BaseController
{

    private $data;
    
    public function __construct()
    {
        $this->data = new stdClass();
        $this->rezeptModel = $this->model('rezepte');  
        $this->kategorieModel = $this->model('kategorien');      
        $this->data->menuKategorien = $this->kategorieModel->holAlleKategorienfuerMenu();      
    }

    public function index()
    {              
        $this->data->rezepte =  $this->rezeptModel->holAlleAktiveRezepte(1,6);             
        $this->view('unterseiten/startseite', $this->data);
    }

    public function getRezept($rezept_id)
    {        
        $this->data->rezept = $this->rezeptModel->holRezept($rezept_id);
        if( !empty($this->data->rezept) )            
            $this->view('unterseiten/rezeptDetails', $this->data);
        else
            $this->pageNotFound($this->data);                         
    }

    public function getAllRezepte()
    {
        $this->data->rezepte = $this->rezeptModel->holAlleAktiveRezepte();
        $this->view('unterseiten/rezeptListe', $this->data);
    }

    public function getAllRezepteNachKategorie($kategorie_id)
    {        
        $this->data->rezepte = $this->rezeptModel->holAlleAktiveRezepteNachKategorie($kategorie_id);
        $this->data->rezeptKategorie = $this->kategorieModel->holKategorie($kategorie_id); 
        if( !empty($this->data->rezeptKategorie) )
            $this->view('unterseiten/rezeptKategorie', $this->data);
        else
            $this->pageNotFound($this->data);      
        
    }

    public function suchen()
    {
        $suchfeld = post('suchfeld');
        if( !empty($suchfeld) )
        {         
            $this->data->gesuchtesWort = $suchfeld;   
            $this->data->gesuchteRezepte = $this->rezeptModel->suchfeld($suchfeld);
            $this->view('unterseiten/gesuchteRezepte', $this->data);
        }
        else
        {
            $this->sessionFlashData([
                'error' => 'Bitte schreiben Sie das, wonach Sie suchen!'
            ]);
            redirect('/');
        }
    }

    public function listeHinzufuegen()
    {
        if( isLogged() )
        {
            $nutzer_id = getUserId();
            $rezept_id = post('rezept_id');
            $this->data->rezept = $this->rezeptModel->holRezept($rezept_id); 
        
            if( empty($this->data->rezept) )
            {
                redirect('/');                       
            } 
            else
            {                
                if( $this->rezeptModel->bereitsZurListeHinzugefuegt($nutzer_id, $rezept_id) )
                {
                    $this->sessionFlashData([
                        'error' => 'Sie haben bereits dieses Rezept zu Ihrer Liste hinzugefügt!'
                    ]);                 
                }
                else
                {        
                    $zurListeHinzufuegen = $this->rezeptModel->zurListeHinzufuegen($nutzer_id, $rezept_id);
                    
                    if($zurListeHinzufuegen)
                    {
                        $this->sessionFlashData([
                            'success' => 'Das Rezept wurde erfolgreich zu Ihrer Liste hinzugefügt!'
                        ]);                     
                    }
                    else
                    {
                        $this->sessionFlashData([
                            'error' => 'Bei der Registrierung in der Datenbank ist ein Problem aufgetreten!'
                        ]);                        
                    }                              
                }
            } 
            $this->view('unterseiten/rezeptDetails', $this->data);
        }   
        else
        {
            redirect('/login');
        }       
    }

    public function meineRezeptliste()
    {
        if( isLogged() )
        {            
            $nutzer_id = getUserId();
            $this->data->rezepte = $this->rezeptModel->holAlleAktiveRezeptefuerNutzer($nutzer_id);
            $this->view('unterseiten/meineRezeptliste', $this->data);          
        }
        else
        {
            redirect('/');
        }
    }

    public function vonListeLoeschen()
    {
        if( isLogged() )
        {
            $rezept_id = post('rezept_id');
            $nutzer_id = getUserId();
            
            $ergebnis = $this->rezeptModel->vonListeLoeschen($rezept_id, $nutzer_id);
            $this->data->rezepte = $this->rezeptModel->holAlleAktiveRezeptefuerNutzer($nutzer_id);
            if( $ergebnis )
            {
                $this->data->erfolg = 'Rezept wurde aus Ihrer Liste erfolgreich gelöscht!';
            }
            else
            {
                $this->data->errors[] = 'Es ist ein Problem aufgetreten!';
            }       
    
            $this->view('unterseiten/meineRezeptliste', $this->data); 
        }
        else
        {
            redirect('/');
        }       
    }



}