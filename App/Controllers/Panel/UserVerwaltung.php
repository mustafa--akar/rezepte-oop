<?php

namespace App\Controllers\Panel;

use App\Core\Controller as BaseController;
use \stdClass;
class UserVerwaltung extends BaseController
{
    private $data;

    public function __construct()
    {
        if( false === isLogged() || false === isAdmin() )
        {
            redirect('/');
        }
        else
        {
            $this->data = new stdClass();
            $this->rezeptModel = $this->model('rezepte');   
            $this->userModel = $this->model('users');
            $this->kategorieModel = $this->model('kategorien'); 

            $this->data->menuKategorien = $this->kategorieModel->holAlleKategorienfuerMenu();           
        }     
    }

    public function userlist()
    {
        $this->data->users = $this->userModel->holAlleNutzer();
        $this->view('panel/userlist', $this->data);
    }

    public function userLoeschen()
    {
        if( isset($_POST['nutzer_id']) )
        {
            $user_id = $_POST['nutzer_id'];
            $userLoeschen = $this->userModel->userLoeschen($user_id);
            if($userLoeschen)
            {
                $this->sessionFlashData([
                    'success' => 'Nutzer wurde erfolgreich gelöscht!'
                ]);                
            }
            else
            {
                $this->sessionFlashData([
                    'error' => 'In der Datenbank ist ein Problem aufgetreten!'
                ]);               
            }  
            return redirect('/verwaltung/users');          
        }
        
    }

    public function userBearbeitenFormular(int $user_id)
    {
        $this->data->user = $this->userModel->holNutzer($user_id);
        if( !empty($this->data->user) )
            $this->view('panel/userBearbeiten', $this->data);
        else
            $this->pageNotFound($this->data);            
    }

    public function neuUserFormular()
    {
        $this->view('panel/userAnlegen', $this->data);
    }

    public function userAnlegen()
    {
		$vorname 	      = post('vorname');
		$nachname 		  = post('nachname');
		$email   		  = post('email');
		$kennwort         = post('kennwort');
		$confirm_kennwort = post('confirm_kennwort');	
		$admin = isset($_POST['admin']) ? 1 : 0;

		if( empty($vorname) || empty($nachname) ||
		   empty($email) || empty($kennwort) || empty($confirm_kennwort) )
		{
            $this->data->errors[] = 'Bitte füllen Sie die Formular vollständig aus !';
            $this->neuUserFormular();  
		} 
		else
		{
			if( strlen($kennwort) < 5 )
			{
				$this->data->errors[] = 'Das Kennwort muss aus mindestens 5 Zeichen bestehen !';
			}

			if( $kennwort !== $confirm_kennwort )
			{
				$this->data->errors[] =  'Kennwort und Confirm-Kennwort sollten übereinstimmen !';
				
			}
			if( !empty($vorname) && !empty($nachname) &&
				!empty($email) && !empty($kennwort) && !empty($confirm_kennwort) &&
				$kennwort === $confirm_kennwort && strlen($kennwort) > 4
			  )				
			{

				if( $this->userModel->holNutzerPerMail($email) )
				{
                    $this->data->errors[] = 'Dieser User hat bereits registiriert!';
                    $this->neuUserFormular();  
				}
				else
				{
					$neuUser = [

							'vorname'	=> $vorname,
							'nachname'	=> $nachname,
							'email'  	=> $email,
							'kennwort'	=> password_hash($kennwort, PASSWORD_DEFAULT),
							'admin'     => $admin,				
							'angelegtes_datum' => date("Y-m-d H:i:s")

					];
                    $userAnlegen = $this->userModel->userAnlegen($neuUser);

                    if($userAnlegen)
                    {
                        $this->sessionFlashData([
                            'success' => 'Ein neuer User erfolgreich angelegt!'
                        ]);                       
                    }
                    else
                    {
                        $this->sessionFlashData([
                            'error' => 'Bei der Registrierung in der Datenbank ist ein Problem aufgetreten!'
                        ]);                      
                    }
                    return redirect('/verwaltung/users');										
                }
            }
            else
            {
                $this->neuUserFormular();                  
            }
        } 
           
    }

    public function userBearbeiten()
    {
        $user_id   = post('user_id');       
        $vorname   = post('vorname');
        $nachname  = post('nachname');
        $email     = post('email');      
        $admin     = isset($_POST['admin']) ? 1 : 0;

        $this->data->errors = [];    
        if( empty($vorname) || empty($nachname) || empty($email) )
        {
            $this->data->errors[] = 'Bitte füllen Sie die Formular vollständig aus !';
            $this->userBearbeitenFormular($user_id); 
        }
        else
        {
            $user = [
                 'vorname'   => $vorname,
                 'nachname'  => $nachname,
                 'email'     => $email,
                 'admin'     => $admin                 
            ];
            $ergebnis = $this->userModel->userBearbeiten($user, $user_id);

            if($ergebnis)
            {
                $this->sessionFlashData([
                    'success' => 'User wurde erfolgreich aktualisiert!'
                ]);             
            }
            else
            {
                $this->sessionFlashData([
                    'error' => 'Bei der Aktualisierung in der Datenbank ist ein Problem aufgetreten!'
                ]);               
            }  
            return redirect('/verwaltung/users');           
        }              
    }

}