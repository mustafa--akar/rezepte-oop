<?php

namespace App\Controllers\Panel;

use App\Core\Controller as BaseController;
use \stdClass;

class RezepteVerwaltung extends BaseController
{

    private $data;

    public function __construct()
    {
        if( false === isLogged() || false === isAdmin() )
        {
            redirect('/');
        }
        else
        {
            $this->data = new stdClass();
            $this->rezeptModel = $this->model('rezepte');   
            $this->userModel = $this->model('users');
            $this->kategorieModel = $this->model('kategorien'); 

            $this->data->menuKategorien = $this->kategorieModel->holAlleKategorienfuerMenu();           
        }         
    }

    public function index()
    {
        $this->data->anzahlRezepte = count( $this->rezeptModel->holAlleRezepte() );
        $this->data->anzahlNutzer  = count( $this->userModel->holAlleNutzer() );
        $this->data->anzahlKategorien = count( $this->kategorieModel->holAlleKategorien() );
        $this->view('panel/startseite', $this->data);
    }

    public function rezeptlist(int $currentPage = 1)
    {
        $limit = 4;
        $offset = ($currentPage - 1) * $limit;
        $this->data->rezepte = $this->rezeptModel->holAlleRezepteMitPaginate($offset, $limit);
        $countPage = ceil(count( $this->rezeptModel->holAlleRezepte() ) / $limit);
        $link = BASE_URI. 'verwaltung/rezepte';
        $this->data->links   = createLinks($countPage, $link, $currentPage);
        $this->view('panel/rezeptliste', $this->data);
    }

    public function neuRezeptFormular()
    {
        $this->data->kategorien = $this->kategorieModel->holAlleKategorien();
        $this->view('panel/rezeptAnlegen', $this->data);
    }

    public function rezeptBearbeitenFormular(int $rezept_id)
    {
        $this->data->rezept = $this->rezeptModel->holRezept($rezept_id);
        if( !empty($this->data->rezept) )
        {
            $this->data->kategorien = $this->kategorieModel->holAlleKategorien();
            $this->data->kategorie_ids_vom_rezept = [];
    
            foreach($this->data->rezept['kategorien'] as $rezept_kategorie)
            {
                $this->data->kategorie_ids_vom_rezept[] = $rezept_kategorie['id'];       
            }         
            
            $this->view('panel/rezeptBearbeiten', $this->data);
        }
        else
        {
            $this->pageNotFound($this->data);            
        }
        
    }

    public function rezeptAnlegen()
    {
        $bezeichnung   = post('bezeichnung');
        $beschreibung  = post('beschreibung');
        $zutaten       = post('zutaten');
        $gewaehlteKategorien    = isset( $_POST['kategorien'] ) ? post('kategorien') : [];        
        $ist_aktiv     = isset( $_POST['aktiv'] ) ? 1 : 0;
        
    
        if( empty($bezeichnung) || empty($beschreibung) || empty($zutaten) )
        {
            $this->data->errors[] = 'Bitte füllen Sie die Formular vollständig aus !';
        }
    
        if( isset( $_FILES['bilder'] ) && $_FILES['bilder']['size'][0] > 0 ) // hat Bilder hochgeladen ?
        {
    
            $erlaubteTypen = [
                'image/png',
                'image/jpg',
                'image/jpeg'
            ];
            
            foreach( $_FILES['bilder']['type'] as $type )
            {            
                if( !in_array($type, $erlaubteTypen) )
                {
                    $this->data->errors[] = 'Ungültiger Dateityp! ';
                } 
            }     
            foreach( $_FILES['bilder']['size'] as $size )
            {
                if( $size > 1000000 )
                {
                    $this->data->errors[] = 'Datei ist zu gross';
                }      
            }
            
            if( !isset($this->data->errors) )
            {
                $bildpfade = [];
                foreach( $_FILES['bilder']['tmp_name'] as $quelle )
                {
                    $zielName = uniqid(). '.jpg';
                    move_uploaded_file( $quelle, BASE_DIR . '/assets/uploads/'. $zielName);
                    $bildpfade[] = $zielName;
                }   
                
                $neuRezepte = [
                    'bezeichnung'   => $bezeichnung,
                    'beschreibung'  => $beschreibung,
                    'zutaten'       => $zutaten,
                    'bilder'        => json_encode($bildpfade),                    
                    'ist_aktiv'     => $ist_aktiv,
                    'angelegtes_datum' => date("Y-m-d H:i:s")
                ];

                $rezeptAnlegen = $this->rezeptModel->rezeptAnlegen($neuRezepte, $gewaehlteKategorien);
                
                if($rezeptAnlegen)
                {
                    $this->sessionFlashData([
                        'success' => 'Ein neues Rezept wurde erfolgreich angelegt'
                    ]);                  
                }
                else
                {
                    $this->sessionFlashData([
                        'error' => 'Bei der Registrierung in der Datenbank ist ein Problem aufgetreten!'
                    ]);                   
                }
                return redirect('/verwaltung/rezepte');   
            }
            else
            {
                $this->neuRezeptFormular();
            }                    
        }
        else
        {
            $this->data->errors[] = 'Bitte wählen Sie mindestens ein Bild aus';
            $this->neuRezeptFormular();
        }                
    }

    public function rezeptLoeschen()
    {
        if( isset($_POST['rezept_id']) )
        {
            $rezept_id = post('rezept_id');

            $rezept = $this->rezeptModel->holRezept($rezept_id);  
            $rezeptLoeschen = $this->rezeptModel->rezeptLoeschen($rezept);
            
            if($rezeptLoeschen)
            {
                $this->sessionFlashData([
                    'success' => 'Rezept wurde erfolgreich gelöscht!'
                ]);                
            }
            else
            {
                $this->sessionFlashData([
                    'error' => 'In der Datenbank ist ein Problem aufgetreten!'
                ]);               
            }           
        }
        return back();                        	        
    }

    public function rezeptBearbeiten(int $rezept_id)
    {
        $rezept = $this->rezeptModel->holRezept(post('rezept_id'));      
        
        $bezeichnung   = post('bezeichnung');
        $beschreibung  = post('beschreibung');
        $zutaten       = post('zutaten');
        $kategorien    = post('kategorien');
        $ist_aktiv     = isset($_POST['aktiv']) ? 1 : 0;      
        
    
        if( empty($bezeichnung) || empty($beschreibung) || empty($zutaten) )
        {
            $this->data->errors[] = 'Bitte füllen Sie die Formular vollständig aus !';
        }
    
        if( isset( $_FILES['bilder'] ) && $_FILES['bilder']['size'][0] > 0 ) // hat Bilder hochgeladen ?
        {
           
            $erlaubteTypen = [
                'image/png',
                'image/jpg',
                'image/jpeg'
            ];
            
            foreach( $_FILES['bilder']['type'] as $type )
            {            
                if( !in_array($type, $erlaubteTypen) )
                {
                    $this->data->errors[] = 'Ungültiger Dateityp! ';
                } 
            }     
            foreach( $_FILES['bilder']['size'] as $size )
            {                
                if( $size > 1000000 )
                {
                    $this->data->errors[] = 'Datei ist zu gross';
                }      
            }
            
            if( !isset($this->data->errors) )
            {
                $bildpfade = [];
                foreach( $_FILES['bilder']['tmp_name'] as $quelle )
                {                    
                    $zielName = uniqid(). '.jpg';
                    move_uploaded_file( $quelle, BASE_DIR . '/assets/uploads/'. $zielName);
                    $bildpfade[] = $zielName;
                }   

                foreach( $rezept['bilder'] as $alteBild )
                {
                    if( file_exists(BASE_DIR. '/assets/uploads/'. $alteBild) )
                    {                        
                         unlink(BASE_DIR. '/assets/uploads/'. $alteBild);
                    }
                }
                
                $bearbeitetesRezept = [
                    'bezeichnung'   => $bezeichnung,
                    'beschreibung'  => $beschreibung,
                    'zutaten'       => $zutaten,
                    'bilder'        => json_encode($bildpfade),                    
                    'ist_aktiv'     => $ist_aktiv                   
                ];
                $rezeptBearbeiten = $this->rezeptModel->rezeptBearbeiten($bearbeitetesRezept, $rezept, $kategorien);
                
                if($rezeptBearbeiten)
                {
                    $this->sessionFlashData([
                        'success' => 'Rezept wurde erfolgreich aktualisiert'
                    ]);                  
                }
                else
                {
                    $this->sessionFlashData([
                        'error' => 'Bei der Aktualisierung in der Datenbank ist ein Problem aufgetreten!'
                    ]);                   
                }                
                return redirect('/verwaltung/rezepte');                    
            }
            else
            {
                $this->rezeptBearbeitenFormular($rezept['id']); 
            }
        }
        else
        {
            if( !isset($this->data->errors) )
            {
                $bearbeitetesRezept = [
                    'bezeichnung'   => $bezeichnung,
                    'beschreibung'  => $beschreibung,
                    'zutaten'       => $zutaten,               
                    'ist_aktiv'     => $ist_aktiv                                     
                ];
    
                $rezeptBearbeiten = $this->rezeptModel->rezeptBearbeiten($bearbeitetesRezept, $rezept, $kategorien);
    
                if($rezeptBearbeiten)
                {
                    $this->sessionFlashData([
                        'success' => 'Rezept wurde erfolgreich aktualisiert'
                    ]);   
                }
                else
                {
                    $this->sessionFlashData([
                        'error' => 'Bei der Aktualisierung in der Datenbank ist ein Problem aufgetreten!'
                    ]);  
                }                
                return redirect('/verwaltung/rezepte');
            }
            else
            {
                $this->rezeptBearbeitenFormular($rezept['id']); 
            }
        }  
             
    }




}