<?php

namespace App\Controllers\Panel;

use App\Core\Controller as BaseController;
use \stdClass;

class KategorieVerwaltung extends BaseController
{

    private $data;

    public function __construct()
    {
        if( false === isLogged() || false === isAdmin() )
        {
            redirect('/');
        }
        else
        {
            $this->data = new stdClass();
            $this->rezeptModel = $this->model('rezepte');   
            $this->userModel = $this->model('users');
            $this->kategorieModel = $this->model('kategorien'); 

            $this->data->menuKategorien = $this->kategorieModel->holAlleKategorienfuerMenu();           
        }         
    }

    public function kategorielist()
    {
        $this->data->kategorien = $this->kategorieModel->holAlleKategorien();
        $this->view('panel/kategorieliste', $this->data);
    }

    public function neuKategorieFormular()
    {
        $this->view('panel/kategorieAnlegen', $this->data);
    }

    public function kategorieBearbeitenFormular(int $kategorie_id)
    {
        $this->data->kategorie = $this->kategorieModel->holKategorie($kategorie_id);
        if( !empty($this->data->kategorie) )
            $this->view('panel/kategorieBearbeiten', $this->data);
        else
            $this->pageNotFound($this->data);            
    }

    public function kategorieAnlegen()
    {
        $bezeichnung      = post('bezeichnung');
        $im_menu_anzeigen = isset($_POST['im_menu_anzeigen']) ? $_POST['im_menu_anzeigen'] : 0;
    
        if( empty($bezeichnung) )
        {
            $this->sessionFlashData([
                'error' => 'Bitte füllen Sie die Formular vollständig aus !'
            ]);
            
            $this->neuKategorieFormular();
        } 
        else
        {
            $neuKategorie = [
                    'bezeichnung'	   => $bezeichnung,
                    'im_menu_anzeigen' => $im_menu_anzeigen
            ];
            $kategorieAnlegen = $this->kategorieModel->kategorieAnlegen($neuKategorie);
            if($kategorieAnlegen)
            {
                $this->sessionFlashData([
                    'success' => 'Ein neue Kategorie erfolgreich angelegt!'
                ]);               
            }
            else
            {
                $this->sessionFlashData([
                    'error' => 'Bei der Registrierung in der Datenbank ist ein Problem aufgetreten!'
                ]);               
            } 
            return redirect('/verwaltung/kategorien');          
        }        
    }

    public function kategorieLoeschen()
    {
        if( isset($_POST['kategorie_id']) )
        {
            $kategorie_id = post('kategorie_id');            
            $kategorieLoeschen = $this->kategorieModel->kategorieLoeschen($kategorie_id);
            if($kategorieLoeschen)
            {
                $this->sessionFlashData([
                    'success' => 'Kategorie wurde erfolgreich gelöscht!'
                ]);               
            }
            else
            {
                $this->sessionFlashData([
                    'error' => 'In der Datenbank ist ein Problem aufgetreten!'
                ]);               
            }          
        }
        return redirect('/verwaltung/kategorien');       
    }

    public function kategorieBearbeiten()
    {
        $kategorie_id     = post('kategorie_id');       
        $bezeichnung      = post('bezeichnung');
        $im_menu_anzeigen = isset($_POST['im_menu_anzeigen']) ? $_POST['im_menu_anzeigen'] : 0;
            
        if( empty($bezeichnung) )
        {
            $this->sessionFlashData([
                'error' => 'Bitte füllen Sie die Formular vollständig aus !'
            ]);            
            $this->kategorieBearbeitenFormular($kategorie_id); 
        }
        else
        {
            $kategorie = [
                 'bezeichnung'      => $bezeichnung,
                 'im_menu_anzeigen' => $im_menu_anzeigen                 
            ];
            $ergebnis = $this->kategorieModel->kategorieBearbeiten($kategorie, $kategorie_id);
            if($ergebnis)
            {
                $this->sessionFlashData([
                    'success' => 'Kategorie wurde erfolgreich aktualisiert!'
                ]);                 
            }
            else
            {
                $this->sessionFlashData([
                    'error' => 'Bei der Aktualisierung in der Datenbank ist ein Problem aufgetreten!'
                ]);              
            }
            return redirect('/verwaltung/kategorien');             
        }
               
    }

}