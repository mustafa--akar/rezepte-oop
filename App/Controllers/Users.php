<?php

namespace App\Controllers;

use App\Core\Controller as BaseController;
use \stdClass;

class Users extends BaseController
{

    private $data;

    public function __construct()
    {
        $this->data = new stdClass();
        $this->rezeptModel = $this->model('rezepte');
        $this->kategorieModel = $this->model('kategorien');
        $this->userModel = $this->model('users');        
        $this->data->menuKategorien = $this->kategorieModel->holAlleKategorienfuerMenu();         
    }

    public function registerForm()
    {        
        $this->view('unterseiten/register', $this->data);
    }

    public function register()
    {
        
        $vorname 	      = post('vorname');
        $nachname 		  = post('nachname');
        $email   		  = post('email');
        $kennwort         = post('kennwort');
        $confirm_kennwort = post('confirm_kennwort');
           
        if( empty($vorname) || empty($nachname) ||
            empty($email) || empty($kennwort) || empty($confirm_kennwort) )
        {   
            $this->data->errors[] = 'Bitte füllen Sie die Formular vollständig aus !';
        } 
        else
        {
            if( strlen($kennwort) < 4 )
            {
                $this->data->errors[] = 'Das Kennwort muss aus mindestens 5 Zeichen bestehen !';
            }
    
            if( $kennwort !== $confirm_kennwort )
            {
                $this->data->errors[] =  'Kennwort und Confirm-Kennwort sollten übereinstimmen !';
                
            }
            if( !empty($vorname) && !empty($nachname) &&
                !empty($email) && !empty($kennwort) && !empty($confirm_kennwort) &&
                $kennwort === $confirm_kennwort && strlen($kennwort) > 4
                )				
            {
    
                if( $this->userModel->holNutzerPerMail($email) )
                {
                    $this->data->errors[] = 'Sie haben bereits registiriert!';
                }
                else
                {
                    $neuUser = [    
                        'vorname'	=> $vorname,
                        'nachname'	=> $nachname,
                        'email'  	=> $email,
                        'kennwort'	=> password_hash($kennwort, PASSWORD_DEFAULT),
                        'admin'     => 0,				
                        'angelegtes_datum' => date("Y-m-d H:i:s")
                    ];    

                    $userAnlegen = $this->userModel->userAnlegen($neuUser);
                    if( $userAnlegen )
                    {
                        $_SESSION['user']       = $neuUser;
                        $_SESSION['eingeloggt'] = true;				
                        $_SESSION['user']['id'] = $userAnlegen;

                        # Weiterleitung zur Startseite
                        redirect('/');                        
                    }
                    else
                    {
                        $this->data->errors[] = 'Bei der Registrierung in der Datenbank ist ein Problem aufgetreten!';                       
                    }

                }   
            }	
    
        }
        $this->registerForm();
    }

    public function loginForm()
    {
        $this->view('unterseiten/login', $this->data);
    }

    public function login()
    {
        $email     = post('email');
        $kennwort  = post('kennwort');
    
        if( empty($email) || empty($kennwort) )
        {
            $this->data->errors[] = 'Bitte füllen Sie die Formular vollständig aus !';
        }
        else
        {
            $user = $this->userModel->holNutzerPerMail($email);

            if( empty($user) )
            {
                $this->data->errors[] = 'User konnte nicht gefunden werden ! ';
            }
            else
            {
                if( password_verify($kennwort, $user['kennwort']) ) {
                    
                    $_SESSION['user']     = $user;
                    $_SESSION['eingeloggt'] = true;				
    
                    #Weiterleitung zur Startseite
                    redirect('/');                    		
    
                } 
                else
                {
                    $this->data->errors[] = 'Kennwort stimmt nicht !';
                }		
            }		
        }
        $this->view('unterseiten/login', $this->data);       
    }
           
    
 

}