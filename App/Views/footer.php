
</main>
<footer class="bg-light footer">
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-md-4 padding"> 

         <div class="flex">
           <div class="social-icon text-center">
             <a href=""><i class="fab fa-facebook-square color" style="font-size: 32px;"></i> </a>
           </div>
           <div class="social-icon text-center">
             <a href=""><i class="fab fa-twitter-square color" style="font-size: 32px;"></i> </a>
           </div>
           <div class="social-icon text-center">
             <a href=""><i class="fab fa-instagram-square color" style="font-size: 32px;"></i> </a>
           </div>
           <div class="social-icon text-center">
            <a href=""> <i class="fas fa-envelope-square color" style="font-size: 32px;"></i> </a>
           </div>                                 
         </div>           
      </div>
    </div>
    <hr />
    <div class="row justify-content-md-center">
      <div class="col-md-4 text-center">
        © <?= date('Y') ?> Rezept-Anwendung GmbH
      </div>
    </div>
  </div>          
</footer>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="<?= BASE_URI. 'assets/js/main.js' ?>" type="text/javascript"></script>

</body>
</html>