

<!DOCTYPE html>
<html>
<head>
  
	<title>Rezeptanwendung</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?= BASE_URI. 'assets/css/style.css' ?>" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
</head>
<body>
<header class="bg-light">
  <div class="container">  
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="<?= BASE_URI ?>">Rezeptanwendung</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item <?= checkAktiveUrl('') ? 'active' : ''; ?>">
          <a class="nav-link" href="<?= BASE_URI ?>">Startseite </a>
        </li>
        <li class="nav-item <?= checkAktiveUrl('rezepte') ? 'active' : ''; ?>">
          <a class="nav-link" href="<?= BASE_URI. 'rezepte' ?>">Rezepte</a>
        </li>
        <li class="nav-item dropdown <?= checkAktiveUrl('kategorie') ? 'active' : ''; ?>">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Kategorien
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <?php foreach( $menuKategorien as $menuKategorie ): ?>
                <a class="dropdown-item <?= checkAktiveKategorie($menuKategorie['id']) ? 'active' : '' ?>" href="<?= BASE_URI. 'kategorie/'. $menuKategorie['id'] ?>">
                  <?= $menuKategorie['bezeichnung'] ?>                    
                </a>
            <?php endforeach; ?>  
          </div>
        </li>
        <?php if( isLogged() ): ?>

          <?php if( isAdmin() ): ?>
            <li class="nav-item <?= checkAktiveUrl('verwaltung') ? 'active' : ''; ?>">
              <a class="nav-link" href="<?= BASE_URI. 'verwaltung' ?>">Verwaltung </a>
          </li>             
          <?php endif; ?>
          <li class="nav-item <?= checkAktiveUrl('meine-rezeptliste') ? 'active' : ''; ?>">
            <a class="nav-link" href="<?= BASE_URI. 'meine-rezeptliste' ?>">Meine Rezeptliste </a>
          </li>         
          <li class="nav-item <?= checkAktiveUrl('logout') ? 'active' : ''; ?>">
            <a class="nav-link" href="<?= BASE_URI. 'logout' ?>">Logout </a>
          </li>                 
        <?php else: ?>
          <li class="nav-item <?= checkAktiveUrl('register') ? 'active' : ''; ?>">
            <a class="nav-link" href="<?= BASE_URI. 'register' ?>">Register </a>
          </li> 
          <li class="nav-item <?= checkAktiveUrl('login') ? 'active' : ''; ?>">
            <a class="nav-link" href="<?= BASE_URI. 'login' ?>">Login </a>
          </li>
        <?php endif; ?>
      </ul>
  
    </div>
  </nav>

   </div> 
</header>
<main class="main-content">
