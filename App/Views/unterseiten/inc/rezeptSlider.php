    <?php if( !empty($rezept) ): ?>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php $counter = 0; ?>
                <?php foreach( $rezept['bilder'] as $bild ): ?>                    
                    <li data-target="#carouselExampleIndicators" data-slide-to="<?= $counter ?>" class="<?= ($counter == 0) ? 'active' : '' ?>"></li>

                    <?php $counter++; ?>
                <?php endforeach; ?>
            </ol>
            <div class="carousel-inner">
                <?php $counter = 1; ?>
                <?php foreach( $rezept['bilder'] as $bild ): ?>
                    <div class="carousel-item <?= ($counter == 1) ? 'active' : '' ?>">
                        <img class="d-block w-100" src="<?= BASE_URI. 'assets/uploads/'. $bild ?>" alt="<?= $rezept['bezeichnung'] ?>">
                    </div>
                    <?php $counter++; ?>
                <?php endforeach; ?>

            </div>
            <?php if(count($rezept['bilder']) > 1): ?>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Vorherig</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Nächste</span>
                </a>
            <?php endif; ?>    
        </div>
        <div class="rezept-kategorie-container">
            <?php foreach( $rezept['kategorien'] as $kategorie ): ?>
                <a class="rezept-kategorie color" href="<?= BASE_URI. 'kategorie/'. $kategorie['id'] ?>"> <i class="fas fa-tags" style="font-size: 16px;"></i> <?= $kategorie['bezeichnung'] ?> </a>  
            <?php endforeach; ?>     
        </div>    
    <?php endif; ?>        
   