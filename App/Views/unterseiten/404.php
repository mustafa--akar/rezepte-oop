    <div class="container mt-30">
	    <div class="row justify-content-md-center">
            <div class="col-sm-12 col-md-6 text-center mt-30 mb-30 p-40">
                
                <h1 class="page-not-found-title"> 404 </h1> 
                <div class="page-not-found-text">Seite nicht gefunden !</div> 
            </div>
        </div>
    </div>