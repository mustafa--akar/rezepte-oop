
<div class="container mt-30">
    <div class="row">
        <?php if( !empty($rezept) ): ?>
            <div class="col-md-6">
                <?php include 'inc/rezeptSlider.php'; ?>
            </div>
            <div class="col-md-6">
                <?= printMessage() ?>
                
                <div class="row rezept-details-title">
                    <div class="col-md-6">
                        <h3> <?= $rezept['bezeichnung'] ?> </h3>
                    </div>
                    <div class="col-md-6">
                        <form action="" method="post">  
                            <?= csrf_token() ?>                          
                            <input type="hidden" name="rezept_id" value="<?= $rezept['id'] ?>">                            
                            <button class="btn btn-info liste-hinzufuegen btn-block">
                                <i class="far fa-plus-square"></i> Zu meiner Liste hinzufügen                               
                            </button>                 
                        </form>
                                
                    </div>
                </div>            

                
                <h5> Zutaten </h5>
                <p> <?= $rezept['zutaten'] ?> </p>
                <hr />
                <h5> Beschreibung </h5>
                <p> <?= $rezept['beschreibung'] ?> </p>
            </div> 
            <?php else: ?>
                <div class="alert alert-danger col-md-12" role="alert">
                    Rezept wurde nicht gefunden !
                </div>
            <?php endif; ?>
    </div>
</div>    