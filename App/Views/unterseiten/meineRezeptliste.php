
<div class="container mt-30">
    <div class="row">
        <?php if( count($rezepte) < 1 ): ?>
            <div class="col-md-12">
                <div class="alert alert-secondary" role="alert">
                  Noch haben Sie kein Rezept zu Ihrer Liste hinzugefügt!
                </div>                
            </div>
        <?php else: ?>  

        <?php if( isset($errors) && !empty($errors) ): ?>
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <?php foreach( $errors as $error ): ?>
                            <li> <?= $error ?> </li>
                        <?php endforeach; ?>
                    </ul>   
                </div>
            </div>
            <?php endif; ?> 

            <?php if( isset($erfolg) ): ?>
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        <?= $erfolg ?>
                    </div>
                </div>
            <?php endif; ?> 

            <?php foreach($rezepte as $rezept): ?>               
                <div class="col-md-4">
                    <div class="card mb-30">
                        <img class="card-img-top" src="<?= BASE_URI. '/assets/uploads/'. $rezept['bilder'][0] ?>" alt="<?= $rezept['bezeichnung'] ?>">
                        <div class="card-body">
                            <h5 class="card-title"><?= $rezept['bezeichnung'] ?></h5>
                            <p class="card-text"><?= substr($rezept['beschreibung'],0,100). '...' ?></p>
                            <a href="<?= BASE_URI. 'rezept/'. $rezept['id'] ?>" class="btn btn-primary btn-sm">Detailansicht</a>                
                            <button type="button" 
                                 dataId="<?= $rezept['id'] ?>"
                                 class="btn btn-danger btn-sm loeschenButton" 
                                 data-toggle="modal" data-target="#loeschenModal"> Löschen 
                            </button>                
                        </div>
                     </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

</div>



<!-- Löschen Modal -->
<div class="modal fade" id="loeschenModal" tabindex="-1" role="dialog" aria-labelledby="loeschenModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sind Sie sicher ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Nach der Bestätigung wird das Rezept aus Ihrer Liste gelöscht!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nein</button>
        <form action="" method="post"> 
           <?= csrf_token() ?>                   
           <input type="hidden" name="rezept_id" value="" id="zuLoeschendeId">
           <button type="submit" id="loeschenLink" class="btn btn-primary">Ja</button>
        </form>
        
      </div>
    </div>
  </div>
</div>