

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-sm-12 col-md-6">
			<?php if( isset($errors) ): ?>
				<div class="alert alert-danger mt-30" role="alert">
					<ul>
						<?php foreach( $errors as $error ): ?>
							<li> <?= $error ?> </li>
						<?php endforeach; ?>
					</ul>	
				</div>
			<?php endif; ?>				
			<div class="form-container">
				<h3>Registrierungsformular</h3>
				<form action="" method="post">
					<?= csrf_token() ?>
					<div class="form-group">
						<label for="vorname">Vorname</label>
						<input type="text" name="vorname" class="form-control" id="vorname" placeholder="Geben Sie bitte Ihren Vornamen ein">
					</div>
					<div class="form-group">
						<label for="nachname">Nachname</label>
						<input type="text" name="nachname" class="form-control" id="nachname" placeholder="Geben Sie bitte Ihren Nachnamen ein">
					</div>				  	
					<div class="form-group">
						<label for="email">Email</label>				    
						<input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Geben Sie Ihre E-Mail Adresse ein">				 
					</div>

					<div class="form-group">
						<label for="kennwort">Kennwort</label>
						<input type="password" name="kennwort" class="form-control" id="kennwort" placeholder="Geben Sie Ihr Kennwort ein">
					</div>

					<div class="form-group">
						<label for="kennwortVerify">Confirm Kennwort</label>
						<input type="password" name="confirm_kennwort" class="form-control" id="kennwortVerify" placeholder="Geben Sie Ihr Kennwort wieder ein">
					</div>					
								
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>		
		</div>
	</div>
</div>

