
<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-sm-12 col-md-6">
			<?php if( isset($errors) ): ?>
				<div class="alert alert-danger mt-30" role="alert">
					<ul>
						<?php foreach( $errors as $error ): ?>
							<li> <?= $error ?> </li>
						<?php endforeach; ?>
					</ul>	
				</div>
			<?php endif; ?>				
			<div class="form-container">
				<h3>Login Formular</h3>
				<form action="" method="post">
				  <?= csrf_token() ?>
				  <div class="form-group">
				    <label for="email">Email</label>
				    <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Geben Sie Ihre E-Mail Adresse ein">
				 
				  </div>
				  <div class="form-group">
				    <label for="kennwort">Kennwort</label>
				    <input type="password" name="kennwort" class="form-control" id="kennwort" placeholder="Geben Sie Ihr Kennwort ein">
				  </div>
				  	
				  <button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>		
		</div>
	</div>
</div>