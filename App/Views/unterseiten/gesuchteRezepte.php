
<div class="such_feld_container">
	<form action="" class="such_form" method="post">
		<div class="container">
			<div class="row">		
			  <div class="col-md-10">
				  <div class="form-group">				  			    
				    <input type="text" name="suchfeld" class="form-control" value="<?= isset($gesuchtesWort) ? $gesuchtesWort : '' ?>"
			    		   id="suchfeld" placeholder="Suchen Sie hier nach einem Rezept">	 
					<?= csrf_token() ?>
				  </div>		  	
			  </div>
			  <div class="col-md-2">
				  <!-- <input type="hidden" name="modus" value="such_feld">	-->
				  <button type="submit" class="btn btn-primary btn-block">Suchen</button>		  	
			  </div>			
			</div>			
		</div>
	</form>		
</div>

	<div class="container mt-30">
	    <div class="row">
     	     <?php if( !empty($gesuchteRezepte) ): ?>   
                <?php foreach($gesuchteRezepte as $rezept): ?>  
                
                    <div class="col-md-4">
                        <div class="card mb-30">
                            <img class="card-img-top" src="<?= BASE_URI. 'assets/uploads/'. $rezept['bilder'][0] ?>" alt="<?= $rezept['bezeichnung'] ?>">
                            <div class="card-body">
                                <h5 class="card-title"><?= $rezept['bezeichnung'] ?></h5>
                                <p class="card-text"><?= substr($rezept['beschreibung'],0,100). '...' ?></p>
                                <a href="<?= BASE_URI. 'rezept/'.$rezept['id'] ?>" class="btn btn-info btn-sm">Detailansicht</a>                 
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>   
			<?php else: ?>
				<div class="col-md-12">
					<div class="alert alert-danger">
						Kein Rezept wurde gefunden!
					</div>
				</div>

            <?php endif; ?>

	    </div>
        <div class="row justify-content-md-center">
        	<div class="col-md-4 text-center">
        		<a href="<?= BASE_URI. 'rezepte' ?>" class="btn btn-info"><i class="fas fa-concierge-bell" style="font-size: 16px;"></i> Alle Rezepte anzeigen</a>
        	</div>
        </div>	    
	</div>







