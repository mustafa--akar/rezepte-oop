

    <div class="container mt-30">
        <div class="title"> <?= 'Rezepte für Kategorie <b>' .$rezeptKategorie['bezeichnung']. '</b>' ?> </div>
        <div class="row">
            <?php if( count($rezepte) < 1 ): ?>
                <div class="col-md-12">
                    <div class="alert alert-secondary" role="alert">
                    Dieser Kategorie wurde noch kein Rezept hinzugefügt!
                    </div>                
                </div>
            <?php else: ?>          
                <?php foreach($rezepte as $rezept): ?>            
                <div class="col-md-4">
                    <div class="card mb-30">
                        <img class="card-img-top" src="<?= BASE_URI. '/assets/uploads/'. $rezept['bilder'][0] ?>" alt="<?= $rezept['bezeichnung'] ?>">
                        <div class="card-body">
                            <h5 class="card-title"><?= $rezept['bezeichnung'] ?></h5>
                            <p class="card-text"><?= substr($rezept['beschreibung'],0,100). '...' ?></p>
                            <a href="<?= BASE_URI. 'rezept/'. $rezept['id'] ?>" class="btn btn-info btn-sm">Detailansicht</a>              
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

    </div>
