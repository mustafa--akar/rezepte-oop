

<div class="container mt-30">
    <div class="row">
        <div class="col-md-3">
           <?php include 'inc/navbar.php'; ?>
        </div>
        <div class="col-md-9">
			<?= printMessage(); ?>
			<div class="bg-light padding">
                <h3> Bearbeiten die Kategorie mit dem Namen <b> <?= $kategorie['bezeichnung']  ?> </b></h3>
				
				<form action="" method="post">
                    <?= csrf_token() ?>
					<div class="form-group">
						<label for="bezeichnung">Bezeichnung</label>
						<input type="text" name="bezeichnung" value="<?= $kategorie['bezeichnung']  ?>" class="form-control" id="bezeichnung" placeholder="">
					</div>
                    <div class="form-group">
                        <label for="im_menu_anzeigen">Im Menu anzeigen ?</label>
                        <input type="checkbox" name="im_menu_anzeigen" value="1" id="im_menu_anzeigen" class="form-control checkbox-form" <?= ($kategorie['im_menu_anzeigen'] == 1) ? 'checked' : '' ?> > 
                    </div>					
                    <input type="hidden" name="kategorie_id" value="<?= $kategorie['id'] ?>">
							
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>           

        </div>
    </div>

</div>

