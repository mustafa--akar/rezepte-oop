<div class="container mt-30">
    <div class="row">
        <div class="col-md-3">
           <?php include 'inc/navbar.php'; ?>
        </div>
        <div class="col-md-9">
        
            <!-- <h4> <b><?= $_SESSION['user']['vorname'].' '.$_SESSION['user']['nachname'] ?></b> Willkommen in der Verwaltung </h4> -->

            <div class="row">
            	<div class="col-md-3">
            		<a class="box-link" href="<?= BASE_URI. 'verwaltung/rezepte' ?>">
	            		<div class="box">
	            			 <i class="fas fa-concierge-bell" style="font-size: 40px;"></i> 
	            			 <p class="mt-10"><?= $anzahlRezepte . ' Rezepte' ?></p>
	            		</div>            			
            		</a>
            	</div>
            	<div class="col-md-3">
            		<a class="box-link" href="<?= BASE_URI. 'verwaltung/users' ?>">
	            		<div class="box">
	            			 <i class="fas fa-user" style="font-size: 40px;"></i> 
	            			 <p class="mt-10"><?= $anzahlNutzer . ' Users' ?></p>
	            		</div>            			
            		</a>            		
            	</div>
            	<div class="col-md-3">
            		<a class="box-link" href="<?= BASE_URI. 'verwaltung/kategorien' ?>">
	            		<div class="box">
	            			 <i class="fas fa-bars" style="font-size: 40px;"></i> 
	            			 <p class="mt-10"><?= $anzahlKategorien . ' Kategorien' ?></p>
	            		</div>            			
            		</a>            		
            	</div>             	            	            	
            </div>

			<div class="row mt-15">
				<div class="col-md-3">
            		<a class="box-link" href="<?= BASE_URI. 'verwaltung/rezept-anlegen' ?>">
	            		<div class="box">
	            			 <i class="fas fa-plus-circle" style="font-size: 40px;"></i> 
	            			 <p class="mt-10"><?= 'Rezept Anlegen'  ?></p>
	            		</div>            			
            		</a>
            	</div>
            	<div class="col-md-3">
            		<a class="box-link" href="<?= BASE_URI. 'verwaltung/user-anlegen' ?>">
	            		<div class="box">
	            			 <i class="fas fa-user-plus" style="font-size: 40px;"></i> 
	            			 <p class="mt-10"><?= 'User Anlegen' ?></p>
	            		</div>            			
            		</a>            		
            	</div>
            	<div class="col-md-3">
            		<a class="box-link" href="<?= BASE_URI. 'verwaltung/kategorie-anlegen' ?>">
	            		<div class="box">
	            			 <i class="fas fa-plus-square" style="font-size: 40px;"></i> 
	            			 <p class="mt-10"><?= 'Kategorie Anlegen' ?></p>
	            		</div>            			
            		</a>            		
            	</div>			
			</div>

        </div>
    </div>

</div>


