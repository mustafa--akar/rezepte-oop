
            <nav class="navbar bg-light panel-navbar p-0">
                <!-- Links -->
                <ul class="navbar-nav w-100">
                    <li class="nav-item <?= checkAktiveNavMenu('verwaltung') ? 'active' : '' ?>">
                        <a class="nav-link color" 
                           href="<?= BASE_URI. 'verwaltung' ?>">
                            <i class="fas fa-home" style="font-size: 14px;"></i> Dashboard
                        </a>
                    </li>                     
                    <li class="nav-item  <?= checkAktiveNavMenu('rezepte') ? 'active' : '' ?>">
                        <a class="nav-link color"                         
                           href="<?= BASE_URI. 'verwaltung/rezepte' ?>">
                            <i class="fas fa-concierge-bell" style="font-size: 14px;"></i> Rezepte
                        </a>
                    </li>                    
                    <li class="nav-item <?= checkAktiveNavMenu('users') ? 'active' : '' ?>">
                        <a class="nav-link color"
                           href="<?= BASE_URI. 'verwaltung/users' ?>">   
                           <i class="fas fa-user" style="font-size: 14px;"></i> Users
                        </a>
                    </li>
                    <li class="nav-item <?= checkAktiveNavMenu('kategorien') ? 'active' : '' ?>">
                        <a class="nav-link color"                         
                           href="<?= BASE_URI. 'verwaltung/kategorien' ?>">
                           <i class="fas fa-bars" style="font-size: 14px;"></i> Kategorien
                        </a>
                    </li>
  
                </ul>
            </nav>