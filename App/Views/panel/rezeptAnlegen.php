

<div class="container mt-30">
    <div class="row">
        <div class="col-md-3">
            <?php include 'inc/navbar.php'; ?>
        </div>
        <div class="col-md-9">

            <?php if( isset($errors) && !empty($errors) ): ?>
            <div class="alert alert-danger" role="alert">
                <ul>
                    <?php foreach( $errors as $error ): ?>
                        <li> <?= $error ?> </li>
                    <?php endforeach; ?>
                </ul>   
            </div>
            <?php endif; ?> 

			<div class="bg-light padding">
                <h3>Neues Rezept Anlegen </h3>
                <form action="" method="post" enctype="multipart/form-data">
                    <?= csrf_token() ?>
                    <div class="form-group">
                        <label for="bezeichnung">Bezeichnung</label>
                        <input type="text" name="bezeichnung" class="form-control" id="bezeichnung" placeholder="">                    
                    </div>
                    <div class="form-group">
                        <label for="beschreibung">Beschreibung</label>
                        <textarea name="beschreibung" class="form-control" id="beschreibung"></textarea>    
                    </div>
                    <div class="form-group">
                        <label for="zutaten">Zutaten</label>
                        <textarea name="zutaten" class="form-control" id="zutaten"></textarea>    
                    </div>
                    <div class="form-group">
                        <label for="kategorie">Kategorie</label>
                        <select multiple name="kategorien[]" class="form-control">
                            <?php foreach( $kategorien as $kategorie ): ?>
                                <option value="<?= $kategorie['id'] ?>"> <?= $kategorie['bezeichnung'] ?> </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="aktiv">Aktiv oder Passiv</label>
                        <input type="checkbox" name="aktiv" value="1" id="aktiv" class="form-control checkbox-form" checked> 
                    </div>                    

                    <div class="form-group">
                        <label for="bilder">Bilder</label>
                        <input type="file" name="bilder[]" multiple="multiple" class="form-control" id="bilder" placeholder="">                    
                    </div>
                    <input type="hidden" name="modus" value="rezept_anlegen">	
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>        
            
        </div>
    </div>

</div>
