

<div class="container mt-30">
    <div class="row">
        <div class="col-md-3">
           <?php include 'inc/navbar.php'; ?>
        </div>
        <div class="col-md-9">
			<?php if( isset($errors)  && !empty($errors) ): ?>
				<div class="alert alert-danger" role="alert">
					<ul>
						<?php foreach( $errors as $error ): ?>
							<li> <?= $error ?> </li>
						<?php endforeach; ?>
					</ul>	
				</div>
			<?php endif; ?>	

       	
			<div class="bg-light padding">
                <h3> <?= 'Bearbeiten Nutzer mit dem Namen ' . $user['vorname'] ?> </h3>
				
				<form action="" method="post">
					<?= csrf_token() ?>
					<div class="form-group">
						<label for="vorname">Vorname</label>
						<input type="text" name="vorname" value="<?= $user['vorname'] ?>" class="form-control" id="vorname" placeholder="Geben Sie bitte Ihren Vornamen ein">
					</div>
					<div class="form-group">
						<label for="nachname">Nachname</label>
						<input type="text" name="nachname" value="<?= $user['nachname'] ?>" class="form-control" id="nachname" placeholder="Geben Sie bitte Ihren Nachnamen ein">
					</div>				  	
					<div class="form-group">
						<label for="email">Email</label>				    
						<input type="email" name="email" value="<?= $user['email'] ?>" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Geben Sie Ihre E-Mail Adresse ein">				 
                    </div> 
                    <div class="form-group">
                        <label for="admin">Admin ?</label>
                        <input type="checkbox" name="admin" value="1" id="admin" class="form-control checkbox-form" <?= ($user['admin'] == 1) ? 'checked' : '' ?>> 
                    </div>                        

                    
                    <input type="hidden" name="user_id" value="<?= $user['id'] ?>">		
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>           

        </div>
    </div>

</div>
