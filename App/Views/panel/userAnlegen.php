

<div class="container mt-30">
    <div class="row">
        <div class="col-md-3">
           <?php include 'inc/navbar.php'; ?>
        </div>
        <div class="col-md-9">
			<?php if( isset($errors)  && !empty($errors) ): ?>
				<div class="alert alert-danger" role="alert">
					<ul>
						<?php foreach( $errors as $error ): ?>
							<li> <?= $error ?> </li>
						<?php endforeach; ?>
					</ul>	
				</div>
			<?php endif; ?>	

	       	
			<div class="bg-light padding">
                <h3>Registrierungsformular </h3>
				
				<form action="" method="post">
					<?= csrf_token() ?>
					<div class="form-group">
						<label for="vorname">Vorname</label>
						<input type="text" name="vorname" class="form-control" id="vorname" placeholder="Geben Sie bitte Ihren Vornamen ein">
					</div>
					<div class="form-group">
						<label for="nachname">Nachname</label>
						<input type="text" name="nachname" class="form-control" id="nachname" placeholder="Geben Sie bitte Ihren Nachnamen ein">
					</div>				  	
					<div class="form-group">
						<label for="email">Email</label>				    
						<input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Geben Sie Ihre E-Mail Adresse ein">				 
					</div>

					<div class="form-group">
						<label for="kennwort">Kennwort</label>
						<input type="password" name="kennwort" class="form-control" id="kennwort" placeholder="Geben Sie Ihr Kennwort ein">
					</div>

					<div class="form-group">
						<label for="kennwortVerify">Confirm Kennwort</label>
						<input type="password" name="confirm_kennwort" class="form-control" id="kennwortVerify" placeholder="Geben Sie Ihr Kennwort wieder ein">
					</div>
                    <div class="form-group">
                        <label for="admin">Admin ?</label>
                        <input type="checkbox" name="admin" value="1" id="admin" class="form-control checkbox-form"> 
                    </div> 					
								
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>           

        </div>
    </div>

</div>
