<div class="container mt-30">
    <div class="row">
        <div class="col-md-3">
           <?php include 'inc/navbar.php'; ?>
        </div>
        <div class="col-md-9">
            <?= printMessage(); ?>      

            <a href="<?= BASE_URI. 'verwaltung/kategorie-anlegen' ?>" class="btn btn-info btn-lg mb-15"><i class="fas fa-plus-circle" style="font-size: 25px;"></i> Neue Kategorie</a>
            <table class="table table-hover">
            <thead>
                <tr>
                    <th class="text-center">KategorieNr</th>
                    <th class="text-center">Bezeichnung</th> 
                    <th class="text-center">Im Menu anzeigen?</th>               
                    <th class="text-center">Operation</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($kategorien as $kategorie): ?>
                <tr>
                    <td class="text-center"><?= $kategorie['id'] ?></th>
                    <td class="text-center"><?= $kategorie['bezeichnung'] ?></td>
                    <td class="text-center">
                        <?php if($kategorie['im_menu_anzeigen'] == 1): ?>
                            <i class="far fa-check-circle" style="font-size:25px; color: lightgreen;"></i>
                        <?php else: ?>
                            <i class="far fa-times-circle" style="font-size: 25px; color: red"></i>
                        <?php endif; ?>    
                    </td>                    
                    <td class="text-center">
                        <a href="<?= BASE_URI. 'verwaltung/kategorie-bearbeiten/'. $kategorie['id'] ?>" class="btn btn-secondary btn-sm">Bearbeiten</a>
                        <button type="button" 
                            dataId="<?= $kategorie['id'] ?>"
                             class="btn btn-danger btn-sm loeschenButton" 
                             data-toggle="modal" data-target="#loeschenModal"> Löschen 
                        </button>
                        
                    </td>
                </tr>
                <?php endforeach; ?>    
            </tbody>
            </table>

        </div>
    </div>

</div>

<!-- Löschen Modal -->
<div class="modal fade" id="loeschenModal" tabindex="-1" role="dialog" aria-labelledby="loeschenModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sind Sie sicher ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Sie können diesen Vorgang nicht rückgängig machen.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nein</button>
        <form action="" method="post"> 
           <?= csrf_token() ?>          
           <input type="hidden" name="kategorie_id" value="" id="zuLoeschendeId">
           <button type="submit" id="loeschenLink" class="btn btn-primary">Ja</button>
        </form>
        
      </div>
    </div>
  </div>
</div>