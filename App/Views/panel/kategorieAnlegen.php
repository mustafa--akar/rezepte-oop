

<div class="container  mt-30">
    <div class="row">
        <div class="col-md-3">
           <?php include 'inc/navbar.php'; ?>
        </div>
        <div class="col-md-9">
			<?= printMessage() ?>
			<div class="bg-light padding">
                <h3>Eine neue Kategorie anegen </h3>
				
				<form action="" method="post">
                    <?= csrf_token() ?>
					<div class="form-group">
						<label for="bezeichnung">Bezeichnung</label>
						<input type="text" name="bezeichnung" class="form-control" id="bezeichnung" placeholder="">
					</div>
                    <div class="form-group">
                        <label for="im_menu_anzeigen">Im Menu anzeigen ?</label>
                        <input type="checkbox" name="im_menu_anzeigen" value="1" id="im_menu_anzeigen" class="form-control checkbox-form"> 
                    </div>	
					<input type="hidden" name="modus" value="kategorie_anlegen">			
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>           

        </div>
    </div>

</div>
