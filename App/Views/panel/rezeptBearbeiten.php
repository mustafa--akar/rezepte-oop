



<div class="container mt-30">
    <div class="row">
        <div class="col-md-3">
            <?php include 'inc/navbar.php'; ?>
        </div>
        <div class="col-md-9">

            <?php if( isset($errors) && !empty($errors) ): ?>
            <div class="alert alert-danger" role="alert">
                <ul>
                    <?php foreach( $errors as $error ): ?>
                        <li> <?= $error ?> </li>
                    <?php endforeach; ?>
                </ul>   
            </div>
            <?php endif; ?> 

			<div class="bg-light padding">
                <h4> <?= 'Bearbeiten den Datensatz mit dem Namen <b>' . $rezept['bezeichnung'] .' </b>' ?> </h4>
                <form action="" method="post" enctype="multipart/form-data">
                    <?= csrf_token() ?>
                    <div class="form-group">
                        <label for="bezeichnung">Bezeichnung</label>
                        <input type="text" name="bezeichnung" value="<?= $rezept['bezeichnung'] ?>" class="form-control" id="bezeichnung" placeholder="">                    
                    </div>
                    <div class="form-group">
                        <label for="beschreibung">Beschreibung</label>
                        <textarea name="beschreibung" class="form-control" id="beschreibung"><?= $rezept['beschreibung'] ?></textarea>    
                    </div>
                    <div class="form-group">
                        <label for="zutaten">Zutaten</label>
                        <textarea name="zutaten" class="form-control" id="zutaten"><?= $rezept['zutaten'] ?></textarea>    
                    </div>
                    <div class="form-group">
                        <label for="kategorie">Kategorie</label>
                        <select multiple name="kategorien[]" class="form-control">
                            <?php foreach( $kategorien as $kategorie ): ?>
                                <option <?= in_array($kategorie['id'], $kategorie_ids_vom_rezept) ? 'selected' : '' ?> 
                                    value="<?= $kategorie['id'] ?>"> <?= $kategorie['bezeichnung'] ?>
                                 </option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="aktiv">Aktiv oder Passiv</label>
                        <input type="checkbox" name="aktiv" value="1" id="aktiv" class="form-control checkbox-form" <?= ($rezept['ist_aktiv'] == 1) ? 'checked' : '' ?> > 
                    </div>

                    <div class="form-group">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                           <strong>Achtung!</strong>
                           Wenn Sie neue Bilder hochladen, werden die alte hochgeladene Bilder automatisch gelöscht!                    
                        </div>                        
                        <div class="row">
                            <div class="col-md-2 mt-15">
                                 <img src="<?= BASE_URI. '/assets/uploads/'. $rezept['bilder'][0] ?>" width="75px">
                            </div>
                            <div class="col-md-10">
                                <label for="bilder">Bilder</label>
                                <input type="file" name="bilder[]" multiple="multiple" class="form-control" id="bilder" placeholder=""> 
                            </div>                            
                        </div>                      
                   
                    </div>
                    <input type="hidden" name="rezept_id" value="<?= $rezept['id'] ?>">
                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

           
            
        </div>
    </div>

</div>
