<?php

namespace App\Core;

class Route
{
    public static array $routes = [];

    public static array $patterns = [
        '{url}'  => '([0-9a-zA-Z-_]+)',
        '{id}'   => '([0-9]+)'
    ];

    public static bool $hasRoute = false;

    public static function get($path, $callback): void
    {
        self::$routes['get'][$path] = $callback;
    }

    public static function post($path, $callback): void
    {
        self::$routes['post'][$path] = $callback;
    }  
    
    public static function getMethod(): string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    public static function hasRoute()
    {
        if( self::$hasRoute === false )
        {            
            $kategorien = new \App\Models\Kategorien();            
            $menuKategorien = $kategorien->holAlleKategorienfuerMenu();

            $seite = 'unterseiten/404.php';
            $filePfad = BASE_DIR. '/App/Views/main.php';
            if( file_exists($filePfad) )
                require $filePfad;            
        }
    }

    public static function csrfControl(): bool 
    {
        $method = self::getMethod();
        if( $method === 'post' )
        {
            if( isset($_POST['_token']) && $_POST['_token'] === $_SESSION['_token'] )            
                return true;            
            else           
                return false;            
        }
        else
        {
            return true;
        }        
    }

    public static function getUrl(): string
    {
        $dirname  = dirname( $_SERVER['SCRIPT_NAME'] );  
        $basename = basename( $_SERVER['SCRIPT_NAME'] );  # index.php
    
        # dirname und basename werden in der Uri gelöscht
        $request_uri = str_replace( [$dirname, $basename], NULL, $_SERVER['REQUEST_URI'] );        
        return $request_uri;            
    }

    public static function getRedirectUrl(): string
    {
        $dirname  = dirname( $_SERVER['SCRIPT_NAME'] );  
        $basename = basename( $_SERVER['SCRIPT_NAME'] );  # index.php
    
        # dirname und basename werden in der Uri gelöscht
        $redirect_uri = str_replace( [$dirname, $basename], NULL, $_SERVER['REDIRECT_URL'] );
        $redirect_uri = ltrim($redirect_uri, '/');
        return $redirect_uri;            
    }

    public static function run()
    {
        if( self::csrfControl() )
        {
            $method = self::getMethod();
            # Hier suchen wir nach dem Pfad in dem Array $routes['get'] und $routes['post']
            foreach(self::$routes[$method] as $path => $callback)
            {            
                $path = str_replace(array_keys(self::$patterns), array_values(self::$patterns), $path);
        
                $request_uri = self::getUrl();
               
                if( preg_match('@^'. $path .'$@', $request_uri, $parameters) )
                {
                    self::$hasRoute = true;
                    # Funktion preg_match gibt uns als erste Parameter die übereinstimmte Stück. Deswegen löschen wir diese
                    array_shift($parameters);                
    
                    # Wenn $callback eine Funktion ist, dann rufen wir diese Funktion
                    if( is_callable($callback) )
                    {
                        call_user_func_array($callback, $parameters);
                    }
                    elseif( is_string($callback) )
                    {         
                        [$controller, $method] = explode('@', $callback);                    
    
                        # Mögliche Variante Überordner/Controller@Method
                        $geteilteControllerName = explode('/',$controller);
                        
                        $controllerName = '\\App\\Controllers';
                        foreach($geteilteControllerName as $teil)
                        {
                            $controllerName .= '\\'. ucfirst($teil);
                        }
                        $controllerInstanz = new $controllerName();
                        $controllerUndMethod = [
                            $controllerInstanz,
                            $method                        
                        ];       
                        call_user_func_array($controllerUndMethod, $parameters); 
                        # $controllerInstanz = new $controllerName();                 
                        # $controllerInstanz->{$method}($parameters); 
        
                    }
                }                       
            }
            # Wenn nach der Schleife immer noch der Wert von self::$hasRoute false ist, wird 404 Seite angezeigt!
            
            self::hasRoute();
        }
        else
        {
            die('CSRF Error');
        }

    }

}
