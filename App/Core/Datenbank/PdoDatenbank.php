<?php

namespace App\Core\Datenbank;
use PDO;

abstract class PdoDatenbank
{
	public $verbindung;
	public $host = "localhost";
	public $user = "root";
	public $passwort = "";
	public $datenbank = "rezepte";
	
	public function __construct()
	{		
		$this->verbindung = new PDO("mysql:host=".$this->host."; dbname=".$this->datenbank.";", 
									$this->user, 
									$this->passwort,
									
									array(
										PDO::ATTR_ERRMODE 					=> PDO::ERRMODE_WARNING,
										PDO::ATTR_DEFAULT_FETCH_MODE 		=> PDO::FETCH_ASSOC,
										PDO::MYSQL_ATTR_USE_BUFFERED_QUERY 	=> true,
										PDO::MYSQL_ATTR_INIT_COMMAND 		=> "SET NAMES utf8"
									)
									
								);		
	}
	
	public function sql_abfrage(string $befehl, array $daten = [])
	{			
		$prepare = $this->verbindung->prepare($befehl); # Vorbereiten
		$prepare->execute($daten); # Ausführen		
		return $prepare;
	}		
	
	public function einfuegen(string $befehl, array $daten = [])
	{
		$antwort = $this->sql_abfrage($befehl, $daten);
				
		if( $antwort->rowCount() )			
			return $antwort->rowCount();		
		else		
			return false;		
	}		
	
	public function aktualisieren(string $befehl, array $daten = [])
	{
		$antwort = $this->sql_abfrage($befehl, $daten);	
		if($antwort == true)		
			return true;		
		else		
			return false;				
	}	

	public function loeschen(string $befehl, array $daten = [])
	{
		$antwort = $this->sql_abfrage($befehl, $daten);	
		if($antwort == true)		
			return $antwort->rowCount();	
		else		
			return false;				
	}	

	public function lesen(string $befehl, array $daten = [])
	{		
		$antwort = $this->sql_abfrage($befehl, $daten);	
		$datensaetze = $antwort->fetchAll();
		return $datensaetze;		
	}		
}










?>