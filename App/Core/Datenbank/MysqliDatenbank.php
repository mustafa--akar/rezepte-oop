<?php

namespace App\Core\Datenbank;
class MysqliDatenbank
{
	public $verbindung;
	public $host = "localhost";
	public $user = "root";
	public $passwort = "";
	public $datenbank = "rezepte";
	
	public function __construct()
	{
		
		$this->verbindung = mysqli_connect($this->host,$this->user,$this->passwort,$this->datenbank);
		$this->sql_abfrage("SET NAMES utf8");		
	}
	public function __destruct()
	{
		
		mysqli_close($this->verbindung);
	}
	
	public function sql_abfrage($befehl, $daten = array())
	{
		#$ergebnis = mysqli_query($this->verbindung, $befehl);
		
		# ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?
		# Variante 1: nur mit ? Platzhalter
		# Befehl ohne Daten vorbereiten
		/*
		$prepare = $this->verbindung->prepare($befehl);
		if(count($daten) >= 1)
		{
			$prepare->bind_param("s", $daten[0]);	# s = string
			#$prepare->bind_param("s", $daten[1]);
			#$prepare->bind_param("s", $daten[2]);
		}
		# Ausführen
		$prepare->execute();
		*/
		#:p1, :p2, :p2, :p4, :p5
		# VARIANTE 2: mit benannte Platzhalter
		foreach($daten as $schluessel => $wert)
		{
			$daten[$schluessel] = mysqli_real_escape_string($this->verbindung, $wert);
			$befehl = str_replace(":".$schluessel, "'".$wert."'", $befehl);
		}
		
		// Senden
		$ergebnis = mysqli_query($this->verbindung, $befehl);		
		return $ergebnis;
	}		
	
	public function einfuegen($befehl, $daten = array())
	{
		$antwort = $this->sql_abfrage($befehl, $daten);	
		if($this->verbindung->insert_id > 0)
		{		
			return $this->verbindung->insert_id; # der neue Primärschlüssel
		}
		else
		{
			echo "Fehler beim Insert:";
			echo $befehl;
		}
	}		
	public function aktualisieren($befehl, $daten = array())
	{
		$antwort = $this->sql_abfrage($befehl, $daten);	
		if($antwort == true)
		{
			$string = "Änderungen erfolgreich:";
			$string .= $antwort->affected_rows."x Datensätze verändert";
			return $string;
		}
		else
		{
			return "Fehler:".$befehl;
		}		
	}	
	public function loeschen($befehl, $daten = array())
	{
		$antwort = $this->sql_abfrage($befehl, $daten);	
		if($antwort == true)
		{
			$string = "Löschen erfolgreich:";
			$string .= $antwort->affected_rows."x Datensätze gelöscht";
			return $string;
		}
		else
		{
			return "Fehler:".$befehl;
		}		
	}	
	public function lesen($befehl, $daten = array())
	{
		$antwort = $this->sql_abfrage($befehl, $daten);	
		$datensaetze = array();
		while($datensatz = mysqli_fetch_assoc($antwort))
		{
			$datensaetze[] = $datensatz;
		}		
		return $datensaetze;		
	}		
}














?>