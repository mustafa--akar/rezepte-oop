<?php

namespace App\Core;

abstract class Controller
{
    public function view(string $seite, $data = [])
    {
        # $data = is_object($data) ? get_object_vars($data) : $data;       
        $data = is_object($data) ? (array) $data : $data;
        is_array($data) ? extract($data) : NULL;
        
        $seite = $seite. '.php';
        $filePfad = BASE_DIR. '/App/Views/main.php';
        if( file_exists($filePfad) )
            require $filePfad;
    }
    public function model($name)
    {        
        $klasse =  '\\App\\Models\\'. ucfirst(strtolower($name)); 
               
        return new $klasse();
    }

    public function sessionFlashData(array $data)
    {
        foreach($data as $key => $value)
        {
            $_SESSION['flash'][$key] = $value;
        }        
    }

    public function pageNotFound($data)
    {
        header("HTTP/1.0 404 Not Found");
        $this->view('unterseiten/404', $data);
    }

}