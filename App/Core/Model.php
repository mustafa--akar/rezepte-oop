<?php

namespace App\Core;

use App\Core\Datenbank\PdoDatenbank as Datenbank;

class Model extends Datenbank
{
    public string $table = '';  
    public string $orderBy = ''; 
    public string $limit = '';
    public string $offset = '';
    public array $where = [];
    public array $data = [];

    public function __construct()
    {          
       parent::__construct(); 
    }
    
    public function where($column, $value, $operation = '= ')
    {
        $this->where[] = $column. ' ' . $operation . ':' . $column;
        $this->data[$column] = $value;
        return $this;
    }

    public function orderBy($column, $type = 'ASC')
    {
        $this->orderBy = ' ORDER BY '.$column .' '. $type; 
        return $this;      
    }

    public function limit(int $limit)
    {
        $this->limit = ' LIMIT '. $limit;
        $this->verbindung->setAttribute( \PDO::ATTR_EMULATE_PREPARES, false );
        return $this; 
    }

    public function offset(int $offset)
    {
        $this->offset = ' OFFSET '. $offset;
        return $this;
    }

    public function table(string $tableName)
    {
        $this->table = $tableName;
        return $this;
    }

    public function refresh()
    {
        $this->table     = '';
        $this->orderBy   = '';
        $this->limit     = '';
        $this->offset    = '';
        $this->where     = [];
        $this->data      = [];
    }

    public function get()
    {
        $sql = sprintf("SELECT * FROM %s ", $this->table);

        $data = $this->data;
        if ( count($this->where) )
        {
            $sql .= ' WHERE ' . implode(' && ', $this->where);            
        }  
        if( !empty($this->orderBy) )
        {
            $sql .= $this->orderBy;
        } 
        if( !empty($this->limit) )
        {
            $sql .= $this->limit;
        } 
        if( !empty($this->offset) )
        {
            $sql .= $this->offset;
        }                     
        $this->refresh();   
      
        return $this->lesen($sql, $data);
    }

    public function paginate(int $offset, int $limit)
    {        
        $data = $this->data;
        $this->verbindung->setAttribute( \PDO::ATTR_EMULATE_PREPARES, false );

        $sql = sprintf("SELECT * FROM %s ", $this->table);
        if ( count($this->where) )
        {
            $sql .= ' WHERE ' . implode(' && ', $this->where);            
        }  
        if( !empty($this->orderBy) )
        {
            $sql.= $this->orderBy;
        }  
        $sql .= ' LIMIT '. $limit. ' OFFSET '. $offset;    
                       
        $this->refresh();     
        return $this->lesen($sql, $data);        

    }

    public function first()
    {
        $sql = sprintf('SELECT * FROM %s', $this->table);
        if ( count($this->where) )
        {        
            $sql .= ' WHERE ' . implode(' && ', $this->where);
        }

        $query = $this->verbindung->prepare($sql);
        $query->execute($this->data);
        $this->refresh();
        return $query->fetch();        
    }

    public function save(array $newRecord)
    {        
        $columns = implode(', ', array_keys($newRecord));      
        
        $values = [];
        foreach( $newRecord as $key => $value )
        {            
            $values[] = ':'.$key;
        }
        $values = implode(', ', $values);

        $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->table, $columns, $values);
       
        $this->refresh();
        return $this->einfuegen($sql, $newRecord);
    }

    public function remove()
    {
        $sql = sprintf('DELETE FROM %s', $this->table);
        if( count($this->where) )
        {
            $sql .= ' WHERE ' . implode(' && ', $this->where);
            $data = $this->data;
            $this->refresh();
            return $this->loeschen($sql, $data);            
        }
    }

    public function update(array $daten)
    {
        $sql = sprintf('UPDATE %s SET ', $this->table);
        foreach($daten as $key => $value)
        {
            $sql .= $key .' = :'. $key .', ';
            
        }
        $sql = rtrim($sql, ", ");
        if ( count($this->where) )
        {        
            $sql .= ' WHERE ' . implode(' && ', $this->where);
        }   
        $daten = array_merge($daten, $this->data);    
        
        $this->refresh();
        return $this->aktualisieren($sql, $daten);
    }

}