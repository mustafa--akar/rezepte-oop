<?php

use App\Core\Route;

/***************** Unten gibt es die Beispiele *******************/
/*****************************************************************/
/*
 Route::get('/', function(){
    echo 'Hallo Welt';
 });

Route::get('/uye/{url}', function($name){
    echo 'Herzlich Willkommen '. $name;
});

Route::get('/users', 'users@index');
Route::post('/users', 'users@post');
*/

########################## GET ###############################

Route::get('/', 'Rezepte@index');
Route::get('/rezept/{id}', 'Rezepte@getRezept');
Route::get('/rezepte', 'Rezepte@getAllRezepte');
// Route::get('/rezepte/{id}', 'Rezepte@getAllRezepte');
Route::get('/kategorie/{id}', 'Rezepte@getAllRezepteNachKategorie');
Route::get('/register', 'Users@registerForm');
Route::get('/login', 'Users@loginForm');
Route::get('/meine-rezeptliste', 'Rezepte@meineRezeptliste');
Route::get('/logout', function(){
    session_destroy();
    redirect('/');
});

######################## POST ##################################
Route::post('/register', 'Users@register');
Route::post('/login', 'Users@login');
Route::post('/rezept/{id}', 'Rezepte@listeHinzufuegen');
Route::post('/meine-rezeptliste', 'Rezepte@vonListeLoeschen');
Route::post('/', 'Rezepte@suchen');


##################### VERWALTUNG #############################

    ####################   GET  #############################
    Route::get('/verwaltung', 'panel/RezepteVerwaltung@index');
    Route::get('/verwaltung/rezepte', 'panel/RezepteVerwaltung@rezeptlist');
    Route::get('/verwaltung/rezepte/{id}', 'panel/RezepteVerwaltung@rezeptlist');
    Route::get('/verwaltung/rezept-anlegen', 'panel/RezepteVerwaltung@neuRezeptFormular');
    Route::get('/verwaltung/rezept-bearbeiten/{id}', 'panel/RezepteVerwaltung@rezeptBearbeitenFormular');
    Route::get('/verwaltung/users', 'panel/UserVerwaltung@userlist');
    Route::get('/verwaltung/user-anlegen', 'panel/UserVerwaltung@neuUserFormular');
    Route::get('/verwaltung/user-bearbeiten/{id}', 'panel/UserVerwaltung@userBearbeitenFormular');
    Route::get('/verwaltung/kategorien', 'panel/KategorieVerwaltung@kategorielist');
    Route::get('/verwaltung/kategorie-anlegen', 'panel/KategorieVerwaltung@neuKategorieFormular');
    Route::get('/verwaltung/kategorie-bearbeiten/{id}', 'panel/KategorieVerwaltung@kategorieBearbeitenFormular');

    ###################  POST  ##############################
    Route::post('/verwaltung/rezept-anlegen', 'panel/RezepteVerwaltung@rezeptAnlegen');
    Route::post('/verwaltung/rezepte', 'panel/RezepteVerwaltung@rezeptLoeschen');
    Route::post('/verwaltung/rezepte/{id}', 'panel/RezepteVerwaltung@rezeptLoeschen');
    Route::post('/verwaltung/rezept-bearbeiten/{id}', 'panel/RezepteVerwaltung@rezeptBearbeiten');
    Route::post('/verwaltung/users', 'panel/UserVerwaltung@userLoeschen');
    Route::post('/verwaltung/user-anlegen', 'panel/UserVerwaltung@userAnlegen');
    Route::post('/verwaltung/user-bearbeiten/{id}', 'panel/UserVerwaltung@userBearbeiten');
    Route::post('/verwaltung/kategorie-anlegen', 'panel/KategorieVerwaltung@kategorieAnlegen');
    Route::post('/verwaltung/kategorien', 'panel/KategorieVerwaltung@kategorieLoeschen');
    Route::post('/verwaltung/kategorie-bearbeiten/{id}', 'panel/KategorieVerwaltung@kategorieBearbeiten');



##################### AUSFÜHREN #############################
Route::run();

