<?php

namespace App\Models;

use App\Core\Model as Model;

class Rezepte extends Model
{
    public function __construct()
    {
        parent::__construct();       
    }

    public function holAlleRezepte()
    {
        $rezepte = $this->table('rezepte')                       
                        ->get(); 

        $kategorien = new Kategorien();
        foreach( $rezepte as $key => $rezept )
        {
            $rezepte[$key]['kategorien'] = $kategorien->holAlleKategorienfuerRezept($rezept['id']);
            $rezepte[$key]['bilder']     = json_decode($rezept['bilder']);
        }
        return $rezepte;       
    }

    public function holAlleRezepteMitPaginate(int $offset, int $limit)
    {
        $rezepte = $this->table('rezepte')
                        ->paginate($offset, $limit); 

        $kategorien = new Kategorien();
        foreach( $rezepte as $key => $rezept )
        {
            $rezepte[$key]['kategorien'] = $kategorien->holAlleKategorienfuerRezept($rezept['id']);
            $rezepte[$key]['bilder']     = json_decode($rezept['bilder']);
        }
        return $rezepte;       
    }

    public function holAlleAktiveRezepte(int $aktiv = 1, int $limit = 0) : array
    {

        if( $limit == 0 )
        {
            $rezepte = $this->table('rezepte')
                            ->where('ist_aktiv', $aktiv)                            
                            ->get();             
        }
        else
        {     
            $rezepte = $this->table('rezepte')
                            ->where('ist_aktiv', $aktiv)
                            ->orderBy('angelegtes_datum', 'DESC')
                            ->limit($limit)
                            ->get();                    
        } 
        $kategorien = new Kategorien();
        foreach( $rezepte as $key => $rezept )
        {
            $rezepte[$key]['kategorien'] = $kategorien->holAlleKategorienfuerRezept($rezept['id']);
            $rezepte[$key]['bilder']     = json_decode($rezept['bilder']);            
        }
        return $rezepte;
    }

    public function holRezept(int $rezept_id) : array
    {        
        $rezept = $this->table('rezepte')
                       ->where('id', $rezept_id)
                       ->first();
        if($rezept)
        {
            $kategorien = new Kategorien();                
            $rezept['kategorien'] = $kategorien->holAlleKategorienfuerRezept($rezept_id); 
            $rezept['bilder']     = json_decode($rezept['bilder']);
            return $rezept;
        }
        else
        {
            return [];
        }        
        
    }
   
    public function holAlleAktiveRezepteNachKategorie(int $kategorie_id, int $aktiv = 1 ) :array
    {
        $rezepteKategorien = $this->table('rezepte_kategorien')
                                  ->where('kategorie_id', $kategorie_id)
                                  ->get();
        $rezepte = [];
        foreach( $rezepteKategorien as $rezepteKategorie )
        {
            $rezepte[] = $this->holRezept($rezepteKategorie['rezept_id']);
        }
        return $rezepte;
                
    }

    public function bereitsZurListeHinzugefuegt(int $nutzer_id, int $rezept_id): bool
    {
        $usersRezepte = $this->table('rezepte_users')
                             ->where('rezept_id', $rezept_id)
                             ->where('user_id', $nutzer_id)
                             ->first();

        if( $usersRezepte )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function holAlleAktiveRezeptefuerNutzer(int $nutzer_id, int $aktiv = 1): array
    {
        $usersRezepte = $this->table('rezepte_users')
                             ->where('user_id', $nutzer_id)
                             ->get();
        $rezepte = [];
        foreach( $usersRezepte as $usersRezept )
        {
            $rezepte[] = $this->holRezept($usersRezept['rezept_id']);
        }
        return $rezepte;
    }    

    public function zurListeHinzufuegen(int $nutzer_id, int $rezept_id)
    {
        $daten = [
            'user_id'   => $nutzer_id,
            'rezept_id' => $rezept_id
        ];

        $ergebnis = $this->table('rezepte_users')
                         ->save($daten);
        return $ergebnis;
       
    }

    public function vonListeLoeschen(int $rezept_id, int $nutzer_id)
    {
        $ergebnis = $this->table('rezepte_users')
                         ->where('rezept_id', $rezept_id)
                         ->where('user_id', $nutzer_id)
                         ->remove();
        return $ergebnis; 
    }

    public function rezeptAnlegen($neuRezepte, $gewaehlteKategorien): bool
    {      
        $ergebnis = $this->table('rezepte')
                         ->save($neuRezepte);
        if($ergebnis)
        {
            $rezept_id = $this->verbindung->lastInsertId();
            foreach( $gewaehlteKategorien as $gewaehlteKategorie )
            {
                $rezepte_kategorien = [
                    'rezept_id'    => $rezept_id,
                    'kategorie_id' => $gewaehlteKategorie
                ];
                $this->table('rezepte_kategorien')
                     ->save($rezepte_kategorien);                               
            }  
            return true;          
        }
        else
        {
           return false; 
        }    
    }

    public function rezeptLoeschen(array $rezept): bool
    {

        /**************  Die zu diesem Rezept gehörte Bilder löschen  *****************/
        /**********************************************************************************/        
        foreach( $rezept['bilder'] as $alteBild )
        {
            if( file_exists(BASE_DIR. '/assets/uploads/'. $alteBild) )
            {               
                 unlink(BASE_DIR. '/assets/uploads/'. $alteBild);
            }    
        }        
        $ergebnis = $this->table('rezepte')
                         ->where('id', $rezept['id'])
                         ->remove();
        if($ergebnis)
        {
            /********  Die Verbindung zwischen diesem Rezept und Kategorien löschen  **********/                
            /**********************************************************************************/
       
            $beziehungRezepteKategorien = $this->table('rezepte_kategorien')
                                               ->where('rezept_id', $rezept['id'])
                                               ->remove();             

            /********  Die Verbindung zwischen diesem Rezept und Users löschen  **********/                
            /**********************************************************************************/ 
        
            $beziehungRezepteUsers = $this->table('rezepte_users')
                                          ->where('rezept_id', $rezept['id'])
                                          ->remove();                        
            return true;            
        }
        else
        {
            return false;
        }   
      
    }


    public function rezeptBearbeiten(array $bearbeitetesRezept, array $rezept, array $kategorien): bool
    {
        $neuRezept = $this->table('rezepte')
                          ->where('id', $rezept['id'])
                          ->update($bearbeitetesRezept);
        if($neuRezept)
        {                
            /**************  Alte Kategorien löschen  *****************/
            /**********************************************************/
            foreach( $rezept['kategorien'] as $alte_kategorie )
            {
                $this->table('rezepte_kategorien')
                     ->where('rezept_id', $rezept['id'])
                     ->where('kategorie_id', $alte_kategorie['id'])
                     ->remove();
            }
            /**************  Neue Kategorien hinzufügen  ***************/
            /***********************************************************/                    
            foreach( $kategorien as $kategorie )
            {
                $newRecord = [
                    'rezept_id'    => $rezept['id'],
                    'kategorie_id' => $kategorie
                ];
                $this->table('rezepte_kategorien')
                     ->save($newRecord);   
            }            
            return true;            
        }
        else
        {
            return false;
        } 
    }

    public function suchfeld(string $suchfeld) :array
    {      
        $sql = 'SELECT rezepte.*
                FROM rezepte	                
                WHERE rezepte.bezeichnung LIKE :suchfeld	                
                OR rezepte.beschreibung LIKE :suchfeld
                OR rezepte.zutaten LIKE :suchfeld';

        $statement = $this->verbindung->prepare($sql);
        $statement->execute([
            'suchfeld' => '%'.$suchfeld.'%'
        ]);

        if( $statement->rowCount() )
        {
            $gesuchteRezepte = [];
            while( $datensatz = $statement->fetch() )
            {	        	
                $datensatz['bilder'] = json_decode($datensatz['bilder']);   
                $gesuchteRezepte[] = $datensatz;
            }
            return $gesuchteRezepte;

        }
        else
        {
            return [];
        }        
    }    



}
