<?php

namespace App\Models;

use App\Core\Model as Model;

class Users extends Model
{
    public function __construct()
    {
        parent::__construct();       
    }
    
    public function holAlleNutzer() : array
    {
        $users = $this->table('users')
                      ->get();        
        return $users;     
    }

    public function holNutzerPerMail(string $email)
    {
        $user = $this->table('users')
                     ->where('email', $email)
                     ->first();
        return $user;
  
    }
    public function holNutzer(int $nutzer_id) : array
    {       
        $user = $this->table('users')
                     ->where('id', $nutzer_id)
                     ->first();
        if($user)
            return $user;
        else
            return [];
    }

    public function userAnlegen(array $neuUser)
    {
        $ergebnis = $this->table('users')
                         ->save($neuUser);
        if($ergebnis)
            return $this->verbindung->lastInsertId();
        else
            return false;      
    }

    public function userBearbeiten(array $user, int $user_id): bool
    {
        $ergebnis = $this->table('users')
                         ->where('id', $user_id)
                         ->update($user);
        if($ergebnis)
            return true;
        else
            return false;
    }

    public function userLoeschen(int $user_id): bool 
    {
        $ergebnis = $this->table('users')
                         ->where('id', $user_id)
                         ->remove();
        if($ergebnis)
            return true;
        else
            return false;  
    }

}