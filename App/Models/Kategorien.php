<?php

namespace App\Models;

use App\Core\Model as Model;

class Kategorien extends Model
{
    public function __construct()
    {
        parent::__construct();       
    }

    public function holAlleKategorien(): array
    {
        $kategorien = $this->table('kategorien')
                           ->get();
        return $kategorien;
    }

    public function holKategorie(int $kategorie_id): array
    {       
        $kategorie = $this->table('kategorien')
                          ->where('id', $kategorie_id)
                          ->first();  
        if($kategorie)
            return $kategorie;    
        else
            return [];
    }

    public function holAlleKategorienfuerRezept(int $rezept_id): array
    {        
        $rezepteKategorien = $this->table('rezepte_kategorien')
                                  ->where('rezept_id', $rezept_id)
                                  ->get();
        $kategorien = [];
        foreach( $rezepteKategorien as $rezepteKategorie )
        {
            $kategorien[] = $this->holKategorie($rezepteKategorie['kategorie_id']);
        }
        return $kategorien;
    }

    public function holAlleKategorienfuerMenu() : array
    {
        $kategorien = $this->table('kategorien')
                           ->where('im_menu_anzeigen', 1)
                           ->get();
        return $kategorien;
    }     

    public function kategorieAnlegen(array $kategorie): bool
    {
        $ergebnis = $this->table('kategorien')
                         ->save($kategorie);
        if($ergebnis)
            return true;
        else
            return false;        
    }

    public function kategorieLoeschen(int $kategorie_id): bool
    {
        $ergebnis = $this->table('kategorien')
                         ->where('id', $kategorie_id)
                         ->remove();
        if($ergebnis)
        {
            /********  Die Verbindung zwischen dieser Kategorie und Rezepte löschen  **********/
            /**********************************************************************************/
            $this->table('rezepte_kategorien')
                 ->where('kategorie_id', $kategorie_id)
                 ->remove(); 
            return true;
        }
        else
        {
            return false;
        }       
    }

    public function kategorieBearbeiten(array $kategorie, int $kategorie_id): bool
    {
        $ergebnis = $this->table('kategorien')
                         ->where('id', $kategorie_id)
                         ->update($kategorie);
        if($ergebnis)
            return true;
        else
            return false;        
    }
}