<?php
    
    session_start();
    const BASE_DIR = __DIR__;
    
    if (empty($_SESSION['_token']))
    {
        $_SESSION['_token'] = bin2hex(random_bytes(32));
    }
    
    require BASE_DIR. '/config.php';    
    require BASE_DIR. '/autoloader.php';
    require BASE_DIR. '/App/Helpers/appHelper.php';
    require BASE_DIR. '/App/Core/Roote.php';
    require BASE_DIR. '/App/Routes/web.php';


    
?>